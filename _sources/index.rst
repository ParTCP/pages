ParTCP-Dokumentation
====================

.. toctree::
   :maxdepth: 2
   :caption: Inhalt:

   vorbemerkungen
   einleitung
   nachrichten
   maschinen
   cryptosystem
   wahlprotokoll
   dsgvo
   verzeichnisstruktur
   abschottung

Verzeichnisse und Tabellen
==========================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
