Kryptosystem
============

Herzstück der ParTCP-Plattform ist ein asymmetrisches Kryptosystem, das
für das Signieren von Nachrichten und das Verschlüsseln von
Nachrichtenelementen verwendet wird. ParTCP 1.0 spezifiziert folgende
Verfahren für die Umsetzung dieses Kryptosystems:

-  Signieren und Verifizieren mittels Ed25519 bzw. Curve25519 (RFC 7748)
-  Schlüsselaustausch mittels X25519
-  Verschlüsselung mittels AES-256 (CTR-Modus)

Universelle öffentliche Schlüssel
---------------------------------

Um den Umgang mit Teilnehmeridentitäten zu erleichtern, setzt ParTCP das
Vorhandensein von universell einsetzbaren öffentlichen Schlüsseln
voraus. Das heißt, auch wenn das verwendete Kryptosystem (wie in ParTCP
1.0 der Fall) unterschiedliche Schlüsselpaare für das Signieren und das
Verschlüsseln von Daten erfordert, tritt nach außen nur ein einzelner
Schlüssel in Erscheinung.

Um aus den zwei öffentlichen Schlüsseln einen zu machen, wird in ParTCP
1.0 folgende (in Pseudocode ausgedrückte) Formel verwendet:

pubKeyUniversal = base64\_encode( concat( pubKeySign, pubKeyCrypt ) )

Das heißt, die öffentlichen Schlüssel für das Signieren und
Verschlüsseln werden als binäre Bytefolgen aneinandergehängt und dann
mittels base64-Kodierung in eine ASCII-Zeichenfolge umgewandelt.

Ein Trennzeichen zwischen den beiden Schlüsseln ist nicht erforderlich,
da die Längen der Schlüssel feststehen (32 Byte). Um aus dem
zusammengefassten Schüssel die beidenTeilschlüssel zu ermitteln, muss
die Bytefolge nach der base64-Dekodierung an der entsprechenden
Byteposition getrennt werden.

Signaturen
----------

Um eine elektronische Signatur in eine Nachricht einzubinden, wird die
binäre Bytefolge mittels base64-Kodierung in eine ASCII-Zeichenfolge
umgewandelt und dann als eigene Zeile mit dem Präfix *Signature:* am
Anfang der Nachricht eingefügt.

Um eine signierte Nachricht zu verfizieren, wird die erste Zeile
entfernt, um die ursprüngliche Nachricht zu erhalten. Aus der entfernen
Zeile wird das Präfix entfernt und der übrige Teil mittels
base64-Dekodierung in eine binäre Bytefolge umgewandelt.

Vor dem Erstellen und Verifizieren einer Signatur ist sicherzustellen,
dass alle Zeilenumbrüche innerhalb der Nachricht durch einfache
Linefeed-Zeichen (ASCII-Wert 10) repräsentiert werden und sich keine
Carriage-Return-Zeichen (ASCII-Wert 13) in der Nachricht befinden.

Verschlüsselung
---------------

ParTCP setzt beim Umgang mit verschlüsselten Daten voraus, dass diese
für sich selbst stehen und für die Entschlüsselung keine Informationen
erforderlich sind außer der Absenderangabe bzw. des öffentlichen
Schlüssels, der für die Verschlüsselung verwendet wurde. Da eine sichere
AES-Verschlüsselung die Übermittlung eines zufällig erzeugten
Initialisierungsvektors erfordert, definiert ParTCP 1.0 folgende Formel,
um den Initialisierungsvektor in die verschlüsselten Daten einzubinden:

.. code-block::

	encryptedFinal = concat(
		base64_encode( iniVector ),
		':',
		base64_encode( encryptedRaw )
	)

Um die endgültige verschlüsselte Zeichenkette zu erzeugen, werden die
binären Bytefolgen des Initialisierungsvektors und der rohen
verschlüsselten Daten jeweils mittels base64-Kodierung in
ASCII-Zeichenfolgen umgewandelt und dann mit einem Doppelpunkt als
Trennzeichen aneinandergehängt.
