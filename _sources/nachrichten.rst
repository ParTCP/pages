Nachrichten
===========

Im ParTCP-Kontext bezeichnet der Begriff „Nachricht“ ein serialisiertes
Datenobjekt im YAML-Format, das zwischen Client und Server ausgetauscht
wird. Die Attribute dieses Datenobjekts werden als „Nachrichtenelemente“
oder kurz „Elemente“ bezeichnet. Damit ein YAML-String als Nachricht
angesehen wird, muss er ein Element mit dem Namen *Message-Type*
enthalten, das angibt, um welche Art von Nachricht es sich handelt.
Hiervon hängt ab, welche weitere Elemente erwartet werden und wie die
Nachricht verarbeitet wird.

Der Name eines Nachrichtenelements wird stets mit großen
Anfangsbuchstaben geschrieben. Besteht er aus mehreren Wörtern, werden
diese durch Bindestriche miteinander verbunden und jeweils mit großen
Anfangsbuchstaben geschrieben. Enthält ein Element ein Datenobjekt,
werden dessen Attribute in Kleinbuchstaben und mit Unterstrichen als
Worttrenner geschrieben.

Bestimmte Nachrichtenelemente sind unabhängig vom Nachrichtentyp für
bestimmte Zwecke reserviert:

:Message-Type: Typ der Nachricht

:To: Adressat (Empfänger) der Nachricht

:From: Absender der Nachricht

:Signature: digitale Signatur der Nachricht

:Date: Zeitstempel der Nachricht

:Public-Key: universeller öffentlicher Schlüssel des Absenders

:Original-Message: die ursprüngliche Nachricht als String (bei
	Antworten)

Nachrichten werden grundsätzlich in UTF-8-Kodierung erstellt.
Zeilenumbrüche müssen durch Linefeed-Zeichen (LF, ASCII-Wert 10)
repräsentiert werden. Carriage-Return-Zeichen (CR, ASCII-Wert 13) sind
nicht zulässig, auch nicht in der Kombination CR+LF.

Ist der Inhalt eines Elements verschlüsselt, wird dem Namen eine Tilde
(~) angehängt. Enthält eine Nachricht dasselbe Element einmal in
verschlüsselter und einmal in unverschlüsselter Form, wird das
unverschlüsselte Element ignoriert, es sei denn, das verschlüsselte
Element ist nicht entschlüsselbar.


Antworten
---------

Eine Nachricht, die ein Client an einen Server sendet, wird von diesem
wiederum mit einer Nachricht beantwortet, wobei die ursprüngliche
Nachricht im Normalfall in die Antwort eingebunden wird (Element
`Original-Message`). Hat die ursprüngliche Nachricht die Änderung eines
Datenobjekts ausgelöst, wird das neue Objekt vollständig in die Antwort
eingebunden, und die Antwort wird auf dem Server gespeichert.

Kann eine Nachricht nicht verarbeitet werden, antwortet der Server mit einer
Nachricht vom Typ `failure-notice` oder `rejection-notice`. Eine
`failure-notice` zeigt an, dass es auf dem Server ein technisches Problem gibt,
das die Verarbeitung der Anfrage unmöglich macht und das nur serverseitig
gelöst werden kann; im Element `Failure-Description` wird in diesem Fall eine
Beschreibung des Fehlers mitgeliefert. Eine `rejection-notice` bedeutet, dass
die Anfrage aufgrund eines fehlerhaften Aufbaus oder ungültiger Daten nicht
verarbeitet werden konnte. In diesem Fall enthält das Element `Rejection-Code`
eine Fehlernummer (siehe folgenden Abschnitt) und das Element
`Rejection-Reason` eine verbale Beschreibung des Fehlers.


Fehlernummern
-------------

Die erste Ziffer einer Fehlernummer gibt die Fehlerklasse an:

:1: fehlerhafter formaler Aufbau der Anfrage
:2: fehlende oder ungültige Angaben
:3: unzureichende Berechtigungen des Absenders
:4: nicht-existierendes oder in falschem Zustand befindliches Datenobjekt
:5: unzureichende Kontingente
:7: kryptografische Fehler
:9: sonstige Fehler

Die meisten Fehlernummern sind nicht global eindeutig, sondern auf einen bestimmten
Nachrichtentyp bezogen. Dieselbe Nummer kann also bei zwei verschiedenen Anfragen
eine unterschiedliche Bedeutung haben. Eine Ausnahme von dieser Regel bilden die
folgenden Nummern:

:10: Anfrage ist kein gültiger YAML-Code
:11: Element `Message-Type` fehlt
:12: Nachrichtentyp wird vom Server nicht unterstützt
:13: Anfrage entspricht nicht dem erwarteten Aufbau
:71: Signatur ist ungültig

