Verzeichnisstruktur
===================

Das folgende Listing zeigt die Verzeichnisstruktur einer beispielhaften
Versammlung mit mehreren Abstimmungen. Die erste Abstimmung
(*abstimmung\_01*) ist bereits beendet. Man sieht, dass der Teilnehmer
mit der Kennung *p000-2298* zweimal abgestimmt hat.

.. code-block::

	├── 20210915-example-event
	│   ├── 20210915-150549-event-definition
	│   ├── 20210918-130030-multi-registration
	│   ├── lotcodes
	│   │   ├── m20000138
	│   │   ├── m20001581

	...

	│   ├── lots
	│   │   ├── M2E0MGRDQ1I5NWEwOgwMjUzZGM2NzZkNDgwZ...ODhhMmQyM2Q1MTUyMzhmMDI3YjA1NTEzMw==
	│   │   │   └── lot
	│   │   ├── NzgyOWZTFiZTllNGFA0NTU2ZDgyOGFmMTIzN...NDJiNzcxN2U5ZDhmOThiZDEyYWJiYjMxMg==

	...

	│   ├── participants
	│   │   ├── p000-0018
	│   │   │   ├── 20210915-164133-registration
	│   │   │   └── 20210916-093351-key-submission
	│   │   ├── p000-0026

	...

	│   └── votings
	│       ├── abstimmung_01
	│       │   ├── 20210917-125309-voting-definition
	│       │   ├── 20210918-103539-vote-count-request
	│       │   └── ballots
	│       │       ├── p000-2298
	│       │       │ ├── ballot-0001
	│       │       │ └── ballot-0002
	│       │       ├── p000-3066

	...

	│       ├── abstimmung_02

	...
