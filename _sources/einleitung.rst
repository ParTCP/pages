Einleitung
==========

Dieses Kapitel gibt einen Überblick über das Wahlprotokoll, das in
ParTCP eingesetzt wird. Der erste Abschnitt geht der Frage nach, welche
Anforderungen erfüllt sein müssen, damit ein Wahlprotokoll als sicher
und vertrauenswürdig eingestuft werden kann. Der zweite Abschnitt
beschreibt die einzelnen Komponenten, auf die bei der Umsetzung des
Protokolls zurückgegriffen wird, bevor dann der letzte Abschnitt den
Ablauf einer Wahl überblicksartig beschreibt. Technische Einzelheiten
finden sich weiter hinten im Abschnitt „Wahlprotokoll“.

Anforderungen
-------------

Unabhängig davon, ob eine Abstimmung durch Urnengang, per Briefwahl oder
auf elektronischem Weg durchgeführt wird, muss das Verfahren einige
grundsätzliche Anforderungen erfüllen:

-  Identifizierbarkeit
   Es muss sichergestellt sein, dass nur berechtigte Personen eine
   Stimme abgeben können und dass eine bestimmte Person dies auch nur
   einmal tun kann. Dazu gehört in der Regel, dass nachweisbar sein
   muss, ob eine bestimmte Person an der Abstimmung teilgenommen hat,
   oder nicht.
-  Anonymität
   Es darf niemandem möglich sein, herauszufinden, *wie* eine bestimmte
   Person abgestimmt hat. Dies gilt für Außenstehende ebenso wie für
   Behörden und Einrichtungen, die an der Durchführung der Wahl
   beteiligt sind. Und es gilt auch für den Abstimmenden selbst: Er darf
   Dritten gegenüber nicht belegen können, wie er abgestimmt hat, da
   sonst nicht auszuschließen ist, dass seine Stimmabgabe durch Anreize
   oder Drohungen beeinflusst wird.
-  Manipulationssicherheit
   Es muss sichergestellt sein, dass jede abgegebene Stimme ins
   Abstimmungsergebnis einfließt. Ein nachträgliches Ändern oder
   Unterdrücken von Stimmen muss ebenso ausgeschlossen sein wie ein
   unbemerktes Hinzufügen zusätzlicher Stimmen.
-  Nachprüfbarkeit
   Wenn es Anhaltspunkte für Fehler gibt, muss es möglich sein, die
   Richtigkeit des Abstimmungsergebnisses zu überprüfen.
-  Zugänglichkeit und Stabilität
   Die Abstimmung muss für alle stimmberechtigten Personen gleichermaßen
   zugänglich sein, also auch für Menschen mit körperlichen
   Beeinträchtigungen. Und es muss weitestgehend sichergestellt sein,
   dass der Abstimmungsverlauf nicht durch Angriffe von außen
   schwerwiegend beeinträchtigt werden kann.
-  Transparenz
   Jeder Beteiligte muss die Möglichkeit haben, alle wesentlichen
   Schritte der Abstimmung zu beobachten und ohne besondere Sachkenntnis
   zu verstehen.

Es gibt kein digitales oder analoges Abstimmungsverfahren, dass all
diese Anforderungen vollständig erfüllt, und auch ParTCP erhebt keinen
solchen Anspruch. Das Ziel ist eher ein Relatives, nämlich in jedem
Punkt mindestens genauso gut, möglichst aber besser abzuschneiden als
eine herkömmliche Briefwahl.

Bausteine
---------

Das Wahlprotokoll, das in ParTCP umgesetzt wird, beruht auf einem
pragmatischen Ansatz. Die Innovation liegt nicht in ausgeklügelten
kryptografischen Verfahren, sondern in einer neuartigen Kombination
bewährter Bausteine, insbesondere asymmetrischer Verschlüsselung,
Einwegverschlüsselung (Hashing) und digitaler Signaturen. Um das
Wahlprotokoll nachvollziehen zu können, ist ein allgemeines Verständnis
dieser Komponenten erforderlich, darum werden diese im Folgenden kurz
dargestellt. Zu beachten ist, dass es sich um vereinfachte bzw.
idealisierte Darstellungen handelt, die lediglich die Grundprinzipien
deutlich machen sollen.

In den folgenden Abschnitten bedeutet die Formulierung „es ist nicht
möglich“, dass es nach dem Stand der Technik eines unverhältnismäßig
hohen Aufwandes an Zeit und Rechnerkapazitäten bedürfte, um den
betreffenden Sachverhalt zu ermöglichen.

Asymmetrische Verschlüsselung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unter Verschlüsselung versteht man allgemein ein Verfahren, bei dem eine
Nachricht („Klartext“) mit Hilfe eines geheimen Schlüssels so verändert
(„verschlüsselt“) wird, dass sie nicht mehr lesbar ist („Geheimtext“).
Der verschlüsselte Inhalt kann nur unter Zuhilfename des Schlüssels
wiedergewonnen („entschlüsselt“) werden.

Lange Zeit hindurch kannte die Menschheit nur sogenannte „symmetrische“
Verschlüsselungsverfahren, bei denen für das Ver- und Entschlüsseln
derselbe Schlüssel verwendet wird. Erst in neuerer Zeit wurden
„asymmetrische“ Verfahren entwickelt, bei denen zwei verschiedene
Schlüssel zum Einsatz kommen: einer für das Ver- und einer für das
Entschlüsseln. Diese Verfahren umgehen das Problem, das mit jeder
symmetrischen Verschlüsselung verbunden ist, nämlich dass man erst
einmal einen sicheren Übertragungsweg braucht, um den geheimen Schlüssel
zwischen den Beteiligten auszutauschen. Hinzu kommt, dass die geheimen
Schlüssel notgedrungen mehreren Personen bekannt sein müssen, was die
Gefahr des Aufdeckens erhöht.

Bei der asymmetrischen Verschlüsselung verbleibt einer der beiden
Schlüssel bei der Person, die das Schlüsselpaar erstellt hat. Dieser
„private“ Schlüssel (private key), manchmal auch „geheimer“ Schlüssel
(secret key) genannt, wird niemals mit anderen Personen ausgetauscht.
Der zweite Schlüssel kann dagegen völlig offen und ungeschützt verteilt
werden, weshalb er auch als der „öffentliche“ Schlüssel (public key)
bezeichnet wird. Wer eine vertrauliche Nachricht senden will, braucht
diese nur mit dem öffentlichen Schlüssel der Zielperson zu verschlüsseln
und kann sicher sein, dass niemand außer dieser Person den Inhalt lesen
kann, da nur sie den für das Entschlüsseln nötigen geheimen Schlüssel
besitzt.

Asymmetrische Verschlüsselungsverfahren gelten als sicher, sind weit
verbreitet und spielen eine zentrale Rolle in der Informationstechnik,
insbesondere beim Online-Banking und beim E-Commerce. Alle modernen
Betriebssysteme liefern die Programmbibliotheken mit, die
Anwendungsprogramme brauchen, um asymmetrische Verschlüsselungsverfahren
nutzen zu können. Für Webbrowser, E-Mail-Clients und viele andere
Programme ist es seit vielen Jahren selbstverständlich, ihren Anwendern
solche Verfahren zugänglich zu machen. Das kleine Schlosssymbol im
Fenster des Webbrowsers, das auf eine (asymmetrisch) verschlüsselte
Verbindung hinweist, ist für die meisten Internetbenutzer ein vertrauter
Anblick.

Ein asymmetrisches Verschlüsselungsverfahren muss folgende Eigenschaften
aufweisen, wenn es für das ParTCP-Wahlprotokoll einsetzbar sein soll:

-  Eine Nachricht, die mit dem öffentlichen Schlüssel eines
   Schlüsselpaars verschlüsselt wurde, lässt sich mit dem zugehörigen
   privaten Schlüssel entschlüsseln.
-  Es ist nicht möglich, eine Nachricht, die mit dem öffentlichen
   Schlüssel eines Schlüsselpaars verschlüsselt wurde, ohne Zugriff auf
   den zugehörigen privaten Schlüssel zu entschlüsseln.
-  Es ist nicht möglich, aus einem öffentlichen Schlüssel den
   zugehörigen privaten Schlüssel zu ermitteln.

Einwegverschlüsselung (Hashing)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Neben der asymmetrischen Verschlüsselung spielt die sogenannte
Einwegverschlüsselung (auch als Hashing oder Hashfunktion bezeichnet)
eine wichtige Rolle im ParTCP-Wahlprotokoll. Hashverfahren sind in der
Informationstechnik noch älter und weiter verbreitet als asymmetrische
Verschlüsselungsverfahren. Sie erzeugen aus einer Nachricht mit Hilfe
eines komplexen mathematischen Verfahrens einen sogenannten Streuwert
(Hash), der überprüfbar ist in dem Sinne, dass dieselbe Nachricht immer
zum selben Streuwert führt. Es ist aber nicht möglich, aus dem Streuwert
wieder die Nachricht zu rekonstruieren.

Ein wichtiges Einsatzgebiet für die Einwegverschlüsselung ist das
Speichern von Passwörtern in Datenbanken. Wenn sich eine Person bei
einem Webserver registriert und dafür einen Benutzernamen und ein
Passwort wählt, wird das Passwort auf dem Server nicht im Klartext
gespeichert, sondern als Streuwert. Ein späterer Anmeldeversuch gelingt
nur, wenn der Streuwert, der aus dem gerade eingegebenen Passwort
errechnet wird, mit dem ursprünglich gespeicherten Streuwert
übereinstimmt. Auf diese Weise ist ein Passwortvergleich möglich, ohne
dass ein Administrator herausfinden kann, welches Passwort für eine
bestimmte Person auf dem Server gespeichert ist.

Ein Einwegverschlüsselungsverfahren muss folgende Eigenschaften
aufweisen, wenn es für das ParTCP-Wahlprotokoll einsetzbar sein soll:

-  Aus einer bestimmten Nachricht wird stets derselbe Streuwert
   errechnet.
-  Die Länge des Streuwerts ist stets gleich und unabhängig von der
   Länge der Nachricht.
-  Es ist nicht möglich, aus dem Streuwert Rückschlüsse auf den Inhalt
   der Nachricht zu ziehen.
-  Es ist extrem unwahrscheinlich, dass bei zwei verschiedenen
   Nachrichten derselben Streuwert errechnet wird und somit praktisch
   unmöglich, zwei unterschiedliche Nachrichten zu finden, die denselben
   Streuwert ergeben.

Eine wichtige Rolle im ParTCP-Konzept spielen sogenannnte *private
Streuwerte*, zu deren Erzeugung neben der eigentlichen Nachricht auch
der private Schlüssel des Servers herangezogen wird. Diese Streuwerte
sind von Außenstehenden nicht nachvollziehbar, das heißt, sie können
selbst dann, wenn der Inhalt einer Nachricht bekannt ist, nicht
herausfinden, welcher Streuwert zu dieser Nachricht gehört.

Digitale Signaturen
~~~~~~~~~~~~~~~~~~~

Digitale Signaturverfahren arbeiten wie asymmetrische
Verschlüsselungsverfahren mit Schlüsselpaaren. Auch hier besitzt jede
Person einen privaten und einen öffentlichen Schlüssel, aber diese
werden nicht benutzt, um eine Nachricht zu ver- und entschlüsseln,
sondern um den Streuwert einer Nachricht (die „digitale Signatur“) zu
erzeugen und zu verifizieren. Der Sender der Nachricht erzeugt mit Hilfe
seines geheimen Schlüssels die digitale Signatur, und der Empfänger kann
mit Hilfe des zugehörigen öffentlichen Schlüssels eine Verfizierung
durchführen, die zweifelsfrei feststellt, ob die Signatur tatsächlich
von dem Sender stammt, oder nicht. Diese Überprüfung schlägt fehl, wenn
eine korrekt signierte Nachricht im Nachhinein inhaltlich verändert
wurde, so dass das digitale Signieren auch einen Schutz vor
Datenmanipulationen darstellt.

Digitale Signaturen, auch elektronische Unterschriften genannt, sind in
der Informationstechnik ähnlich lange bekannt und ähnlich verbreitet wie
asymmetrische Verschlüsselungsverfahren. Sie dienen insbesondere dazu,
die Urheberschaft von E-Mails und Dateien zu dokumentieren bzw.
überprüfbar zu machen. Mit ihrer Hilfe lässt sich zum Beispiel
feststellen, ob eine bestimmte Software, die man sich aus dem Internet
heruntergeladen hat, aus einer vertrauenswürdigen Quelle stammt.

Ein digitales Signaturverfahren muss folgende Eigenschaften aufweisen,
wenn es für das ParTCP-Wahlprotokoll einsetzbar sein soll:

-  Eine Signatur, die mit einem bestimmten geheimen Schlüssel erzeugt
   wurde, lässt sich mit dem zugehörigen öffentlichen Schlüssel
   verifizieren.
-  Es ist extrem unwahrscheinlich, dass für zwei verschiedene
   Nachrichten mit demselben privaten Schlüssel dieselbe Signatur
   erzeugt wird. Das heißt, dass eine Signatur nicht mehr erfolgreich
   verifiziert werden kann, wenn die ursprüngliche Nachricht verändert
   wurde.
-  Es ist nicht möglich, aus einem öffentlichen Schlüssel den
   zugehörigen privaten Schlüssel zu ermitteln.
-  Ohne Zugriff auf den zugehörigen privaten Schlüssel ist es nicht
   möglich, eine digitale Signatur zu erzeugen, die mit einem bestimmten
   öffentlichen Schlüssel verifizierbar ist.

Zufallszahlengenerator
~~~~~~~~~~~~~~~~~~~~~~

Der vierte zentrale Baustein für das ParTCP-Wahlprotokoll ist ein
Mechanismus für das Generieren von Zufallszahlen. Dieser wird an
verschiedenen Stellen verwendet, um nicht-vorhersagbare Zeichenketten zu
erzeugen. Entscheidend sind hier die folgenden Eigenschaften:

-  Bei einer großen Zahl an Ziehungen müssen alle möglichen Werte gleich
   häufig auftreten (Gleichverteilung).
-  Nach der Ziehung eines bestimmten Werts muss der darauffolgende Wert
   ebenfalls gleich häufig verteilt vorkommen (Unvorhersagbarkeit).

Nicht-geheime Abstimmungen
--------------------------

Um den Ablauf einer geheimen Abstimmung zu verstehen, ist es hilfreich,
sich zunächst vor Augen zu führen, wie eine nicht-geheime (namentliche)
Abstimmung mit den oben beschriebenen Komponenten aussehen könnte:

1. Für jeden Teilnehmer wird auf dem Schlüsselserver ein Ordner mit
   einer eindeutigen Kennung (Teilnehmerkennung) angelegt. Diese Kennung
   wird zusammen mit einem zufällig erzeugten Berechtigungscode an den
   Teilnehmer gesendet. Der Streuwert des Berechtigungscodes wird im
   Teilnehmerordner gespeichert.
2. Der Teilnehmer erzeugt ein Schlüsselpaar und sendet seinen
   öffentlichen Schlüssel zusammen mit seiner Teilnehmerkennung und
   seinem Berechtigungscode als signierte Nachricht an den
   Schlüsselserver. Der Server prüft a) anhand der Signatur, ob der
   Absender tatsächlich im Besitz des privaten Schlüssels ist, der zu
   dem öffentlichen Schlüssel gehört, und b) anhand des hinterlegten
   Streuwerts, ob es sich um den korrekten Berechtigungscode handelt.
   Wenn beides zutrifft, wird der Schlüssel im Teilnehmerordner
   abgespeichert und der Berechtigungscode ungültig gemacht, damit keine
   weitere Schlüsselhinterlegung möglich ist.
3. Steht eine Abstimmung an, füllt der Teilnehmer den digitalen
   Stimmzettel aus, signiert diesen mit Hilfe seines privaten Schlüssels
   und sendet ihn an den Abstimmungsserver. Dieser überprüft anhand der
   Teilnehmerkennung, ob es sich um einen stimmberechtigten Teilnehmer
   handelt. Außerdem verifiziert er die Signatur anhand des öffentlichen
   Schlüssels, der auf dem Schlüsselserver hinterlegt ist.
4. Nachdem alle Teilnehmer ihre digitalen Stimmzettel abgegeben haben,
   werden die Stimmen ausgezählt und die Ergebnisse veröffentlicht.

Durch den Einsatz der Teilnehmerkennungen und Signaturen lassen sich gefälschte
Stimmzettel ohne weiteres erkennen und aussondern. Und da es sich um
gläserne Server handelt, kann jeder Beteiligte jeden abgegebenen
Stimmzettel auf seine Richtigkeit überprüfen.

Geheime Abstimmungen
--------------------

In den meisten Fällen darf nicht nachvollziehbar sein, welcher
Teilnehmer wie abgestimmt hat, darum ist das eben beschriebene Verfahren
nicht praxistauglich. Aber es ist lediglich ein zusätzlichen Schritt
erforderlich, um geheime Abstimmungen zu ermöglichen. Dieser zusätzliche
Schritt besteht darin, dass für jede Abstimmung ein Pool an anonymen
Teilnehmerkennungen erzeugt und jedem Stimmberechtigten nach dem
Zufallsprinzip eine dieser Pseudo-Identitäten zugelost wird. Alle
weiteren Schritte laufen dann so ab wie oben beschrieben, das heißt, der
Teilnehmer hinterlegt (incognito) seinen öffentlichen Schlüssel und
sendet seinen digitalen signierten Stimmzettel (ebenfalls incognito,
aber unverschlüsselt) an den Abstimmungsserver. Der Abstimmungsserver
kann anhand der Teilnehmerkennung feststellen, dass es sich um einen
stimmberechtigten Teilnehmer handelt, und er kann auch die Signatur
anhand des öffentlichen Schlüssels verifizieren, aber er hat keine
Möglichkeit festzustellen, welche Person sich hinter dieser anonymen
Identität verbirgt.

Damit dieses Verfahren die weiter obene beschriebenen Anforderungen
erfüllt, müssen zwei Voraussetzungen erfüllt sein:

1. Es darf keine Möglichkeit geben herauszufinden, welchem Teilnehmer
   welche anonyme Abstimmungskennung zugelost wurde.
2. Es muss sichergestellt sein, dass nur stimmberechtigte Teilnehmer
   eine anonyme Abstimmungskennung erhalten.

Damit die Zuordnung einer anonymen Abstimmungskennung zu einem bestimmten
Teilnehmer nicht nachvollziehbar ist, wird jede dieser Kennungen mit einer
zufällig erzeugten „Losnummer“ verknüpft, die an den Abstimmungsleiter
ausgehändigt, aber auf dem Server nur als privater Streuwert gespeichert wird.
Das heißt, der Server kann zu einer bestimmten Losnummer die zugehörige anonyme
Abstimmungskennung ermitteln, aber niemand sonst.

Damit serverseitig keine Verbinung zwischen einem bestimmten Mitglied
und einer Losnummer möglich ist, findet die Verteilung mit Hilfe eines
externen Systems statt, das autonom arbeitet. Der Server liefert
lediglich eine Liste mit den Losnummern aus, und der Versammlungsleiter
verteilt diese nach dem Zufallsprinzip unter den Teilnehmern (siehe
„Übermittlung der Losnummern“ im Abschnitt „Wahlprotokoll > Vorbereitung
einer Veranstaltung“).
